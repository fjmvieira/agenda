package pessoa;

public class PessoaFisica extends Pessoa{

	private String cpf;
	private String sobenome;
	private String email;
	
	public PessoaFisica(String cpf){
		this.cpf = cpf;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getSobenome() {
		return sobenome;
	}
	public void setSobenome(String sobenome) {
		this.sobenome = sobenome;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}
	